"""SwisscomMIP tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th

from tap_swisscom_mip.streams import (
    SwisscomMIPStream,
    AreasStream,
    AreaStream,
    TripsODOriginsStream,
    TripsODDestinationsStream,
    TripsODMatrixStream,
    TripsCountAgeStream,
    TripsCountCountryStream,
    TripsCountGenderStream,
    TripsCountReasonStream,
    TripsCountTotalStream,
    TripsCountTypeStream,
    TripsCountTransporModeStream,
)

STREAM_TYPES = [
    AreasStream,
    AreaStream,
    TripsODOriginsStream,
    TripsODDestinationsStream,
    TripsODMatrixStream,
    TripsCountAgeStream,
    TripsCountCountryStream,
    TripsCountGenderStream,
    TripsCountReasonStream,
    TripsCountTotalStream,
    TripsCountTypeStream,
    TripsCountTransporModeStream,
]


class TapSwisscomMIP(Tap):
    """SwisscomMIP tap class."""
    name = "tap-swisscom-mip"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "auth_token",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="The current bearer token to authenticate against the API service"
        ),
        th.Property(
            "area_id",
            th.StringType,
            required=True,
            description="Area ID to replicate"
        ),
        th.Property(
            "period_start",
            th.StringType,
            required=True,
            description="Monday (YYYY-MM-DD) for period begin"
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://insights-server-prod.scapp.swisscom.com/api",
            description="The base url for the API service"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapSwisscomMIP.cli()
