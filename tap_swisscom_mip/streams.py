"""Stream type classes for tap-swisscom-mip."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_swisscom_mip.client import SwisscomMIPStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")

class AreasStream(SwisscomMIPStream):
    """Define custom stream."""
    name = "areas"
    path = "/areas"
    primary_keys = ["id"]
    replication_key = None
    schema_filepath = SCHEMAS_DIR / "areas.json"

class AreaStream(SwisscomMIPStream):
    """Define custom stream."""
    name = "area"
    primary_keys = ["id"]
    replication_key = None
    schema_filepath = SCHEMAS_DIR / "area.json"

    @property
    def path(self):
        return f"/areas/{self.area_id}"

class TripsStream(SwisscomMIPStream):
    name = "trips"
    primary_keys = ['_type','area_id','period','date','trip_type','reason','transport_mode','interval','aggregationLevel']
    replication_key = None
    records_jsonpath = "$[values][*]"
    rest_method = "POST"
    direction = None
    
    current_type = ""
    MATRIX = []

    page_token = 0

    @property
    def path(self):
        return f"/areas/{self.area_id}/trips/{self.current_type}"

    @property
    def _request_payload(self):
        return {
            "_type": self.MATRIX[self.page_token]['_type'],
            "period": {
                "start": self.period_start_date.date().isoformat(),
                "end": self.period_end_date.date().isoformat()
            },
            "tripTypes": [self.MATRIX[self.page_token]['tripType']],
            "reasons": [self.MATRIX[self.page_token]['reason']],
            "transportModes": [self.MATRIX[self.page_token]['transportMode']],
            "interval": self.MATRIX[self.page_token]['interval'],
            "aggregationLevel": self.MATRIX[self.page_token]['aggregationLevel']
        }

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: int | None
    ) -> Optional[dict]:
        self.page_token = next_page_token or 0
        return self._request_payload

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        flattened_payload = self._request_payload
        flattened_payload = {
            '_type': flattened_payload['_type'],
            'trip_type': flattened_payload['tripTypes'][0],
            'reason': flattened_payload['reasons'][0],
            'transport_mode': flattened_payload['transportModes'][0],
            'interval': flattened_payload['interval'],
            'period': flattened_payload['period'],
            'aggregationLevel': flattened_payload['aggregationLevel'],
        }
        if self.current_type in ['origins','destinations']:
            row['values'] = row[self.current_type]
            row.pop(self.current_type)
        return { 
            **row,
            'area_id': self.area_id,
            'direction': self.direction,
            **flattened_payload
        }
    
    def get_next_page_token(
        self,
        response,
        previous_token: int | None,
    ) -> int | None:

        previous_token = previous_token or 0

        if previous_token < len(self.MATRIX) - 1:
            self.current_type = self.MATRIX[previous_token + 1]['_type']
            return previous_token + 1

        return None
    

class TripsODStream(TripsStream):
    schema_filepath = SCHEMAS_DIR / "od-trips.json"

    records_jsonpath = f"$[values][*]"

class TripsODOriginsStream(TripsODStream):
    name = "trips_origins"
    current_type = 'origins'

    @property
    def MATRIX(self):
        MATRIX = []
        for reason in ['ALL','COMMUTER','OTHER']:
            for transportMode in ['ALL','HIGHWAY','TRAIN','ROAD']:
                for interval in ['MONTH']:
                    for aggregationLevel in ['PLZ','MUN','DIS','CAN']:
                        for tripType in ['INWARD']:
                            MATRIX = MATRIX + [{
                                '_type': 'origins',
                                'reason': reason,
                                'transportMode': transportMode,
                                'interval': interval,
                                'aggregationLevel': aggregationLevel,
                                'tripType': tripType
                            }]
        return MATRIX

class TripsODDestinationsStream(TripsODStream):
    name = "trips_destinations"
    current_type = "destinations"

    @property
    def MATRIX(self):
        MATRIX = []
        for reason in ['ALL','COMMUTER','OTHER']:
            for transportMode in ['ALL','HIGHWAY','TRAIN','ROAD']:
                for interval in ['MONTH']:
                    for aggregationLevel in ['PLZ','MUN','DIS','CAN']:
                        for tripType in ['OUTWARD']:
                            MATRIX = MATRIX + [{
                                '_type': 'destinations',
                                'reason': reason,
                                'transportMode': transportMode,
                                'interval': interval,
                                'aggregationLevel': aggregationLevel,
                                'tripType': tripType
                            }]
        return MATRIX

class TripsODMatrixStream(TripsODStream):
    name = "trips_odmatrix"
    schema_filepath = SCHEMAS_DIR / "od-matrix-trips.json"
    current_type = 'odmatrix'
    primary_keys = ['_type','area_id','period','origin','destination','trip_type','reason','transport_mode','interval','aggregationLevel']

    records_jsonpath = f"$[values][*]"

    @property
    def MATRIX(self):
        MATRIX = []
        for reason in ['ALL','COMMUTER','OTHER']:
            for transportMode in ['ALL','HIGHWAY','TRAIN','ROAD']:
                for interval in ['MONTH']:
                    for aggregationLevel in ['PLZ','MUN','DIS','CAN']:
                        for tripType in ['TRANSIT']:
                            MATRIX = MATRIX + [{
                                '_type': 'odmatrix',
                                'reason': reason,
                                'transportMode': transportMode,
                                'interval': interval,
                                'aggregationLevel': aggregationLevel,
                                'tripType': tripType
                            }]
        return MATRIX

COUNT_TYPES = ["age-counts", "country-counts", "gender-counts", "reason-counts", "type-counts", "transport-mode-counts"]

class TripsCountStream(TripsStream):
    schema_filepath = SCHEMAS_DIR / "counts.json"
    records_jsonpath = f"$[counts][*]"

    @property
    def MATRIX(self):
        return get_matrix(self.current_type)

class TripsCountAgeStream(TripsCountStream):
    name = "trips_counts_age"
    current_type = 'age-counts'

class TripsCountCountryStream(TripsCountStream):
    name = "trips_counts_country"
    current_type = 'country-counts'

class TripsCountGenderStream(TripsCountStream):
    name = "trips_counts_gender"
    current_type = 'gender-counts'

class TripsCountReasonStream(TripsCountStream):
    name = "trips_counts_reason"
    current_type = 'reason-counts'

class TripsCountTotalStream(TripsCountStream):
    name = "trips_counts_total"
    current_type = 'total'

class TripsCountTypeStream(TripsCountStream):
    name = "trips_counts_type"
    current_type = 'type-counts'

class TripsCountTransporModeStream(TripsCountStream):
    name = "trips_counts_transport_mode"
    current_type = 'transport-mode-counts'

def get_matrix(type):
    MATRIX = []
    reasons = ['ALL'] if type == 'reason-counts' else ['ALL','COMMUTER','OTHER']
    for reason in reasons:
        transportModes = ['ALL'] if type == "transport-mode-counts" else ['ALL','HIGHWAY','TRAIN','ROAD']
        for transportMode in transportModes:
            intervals = ['MONTH'] if type in ['age-counts','country-counts','gender-counts'] else ['DAY', 'HOUR']
            for interval in intervals:
                for aggregationLevel in ['CAN']:
                    tripTypes = ['OVERVIEW'] if type == "type-counts" else ['OVERVIEW','INWARD','OUTWARD','TRANSIT','LOCAL']
                    for tripType in tripTypes:
                        MATRIX = MATRIX + [{
                            '_type': type,
                            'reason': reason,
                            'transportMode': transportMode,
                            'interval': interval,
                            'aggregationLevel': aggregationLevel,
                            'tripType': tripType
                        }]
    return MATRIX
