# API docs

This tap requires a manual log-in to https://mip.swisscom.com and extraction of a bearer token in order to fetch all data via the API. The token also might expire and hence needs to be refreshed during a run.

**Recommended workflow**
Run EL steps locally only (as compared to a CI/CD or Airflow runner) and keep updating the bearer token (potentially project ids) via .env file.

```bash
meltano run tap-swisscom-mip target-postgres
```

## Maps

Endpoint: `/areas`

```json
[
  {
    "id": 12345,
    "name": "AOI NAME",
    "publicAccess": false,
    "externalRef": "ucTtGoEBM1f-pDgGnUF0",
    "organizationName": "Organisation"
  }
]
```

Endpoint: `/areas/12345`

```json
{
  "id": 12345,
  "name": " aoi-name",
  "publicAccess": false,
  "area": {
    "type": "POLYGON",
    "shape": [
      {
        "longitude": 8.369651,
        "latitude": 47.361531
      },
      {
        "longitude": 8.372333,
        "latitude": 47.360586
      },
      {
        "longitude": 8.374822,
        "latitude": 47.360019
      }
    ]
  },
  "insights": [
    {
      "id": 12345,
      "name": " aoi-name",
      "offerId": 1234,
      "areaId": 12345,
      "type": "TRAFFIC",
      "price": 1500,
      "periods": [
        {
          "id": 12345,
          "start": "2019-09-16",
          "end": "2019-09-22"
        }
      ],
      "organizationName": "Hosoya",
      "status": "PAID",
      "dataAccessGranted": true,
      "dwellTimeAvailable": false
    }
  ],
  "postalCodes": []
}
```

## Trips

### Total

Endpoint: `/areas/12345/trips/total`

```json
{
  "period": {
    "start": "2019-09-16",
    "end": "2019-09-22"
  },
  "tripTypes": ["OVERVIEW"], # INWARD, TRANSIT, LOCAL
  "reasons": ["ALL"], # COMMUTER, OTHER
  "transportModes": ["ALL"], # HIGHWAY, TRAIN, ROAD
  "interval": "DAY", # DAY + HOUR
  "aggregationLevel": "PLZ" # CAN, DIS, MUN
}
```

```json
{
  "interval": "DAY",
  "values": [
    {
      "date": "2019-08-25T22:00:00.000Z",
      "count": 6932,
      "weightedCount": 9894,
      "values": {},
      "weightedTotal": 9894,
      "total": 6932
    },
    {
      "date": "2019-08-26T22:00:00.000Z",
      "count": 7292,
      "weightedCount": 10485,
      "values": {},
      "weightedTotal": 10485,
      "total": 7292
    },
    ...
  ],
  "counts": [
    {
      "date": "2019-08-25T22:00:00.000Z",
      "count": 6932,
      "weightedCount": 9894,
      "values": {},
      "weightedTotal": 9894,
      "total": 6932
    },
    {
      "date": "2019-08-26T22:00:00.000Z",
      "count": 7292,
      "weightedCount": 10485,
      "values": {},
      "weightedTotal": 10485,
      "total": 7292
    },
    ...
  ]
}
```

### Counts

Endpoints: `/areas/12345/trips/gender-counts`, `/areas/12345/trips/country-counts`, `/areas/12345/trips/age-counts`, `/areas/12345/trips/type-counts`, `/areas/12345/trips/reason-counts`, `/areas/12345/trips/transport-mode-counts`

```json
{
  "period": {
    "start": "2019-09-16",
    "end": "2019-09-22"
  },
  "tripTypes": ["OVERVIEW"], # INWARD, TRANSIT, LOCAL
  "reasons": ["ALL"], # COMMUTER, OTHER
  "transportModes": ["ALL"], # HIGHWAY, TRAIN, ROAD
  "interval": "MONTH", # MONTH
  "aggregationLevel": "PLZ" # CAN, DIS, MUN
}
```

```json
{
  "interval": "MONTH",
  "counts": [
    {
      "date": "2019-07-31T22:00:00.000Z",
      "total": 35643,
      "weightedTotal": 54466,
      "values": {
        "Female": [18289, 27576],
        "Male": [17354, 26889]
      }
    },
    {
      "date": "2019-08-31T22:00:00.000Z",
      "total": 5238,
      "weightedTotal": 7992,
      "values": {
        "Female": [2662, 4025],
        "Male": [2576, 3967]
      }
    }
  ]
}
```

## Trips Origins / Destinations

Endpoints: `/areas/12345/trips/origins` / `/areas/12345/trips/destinations` / `/areas/12345/trips/odmatrix`

```json
{
  "period": {
    "start": "2019-08-26",
    "end": "2019-09-01"
  },
  "tripTypes": ["INWARD"], # OUTWARD for destinations, TRANSIT for odmatrix
  "reasons": ["ALL"],  # COMMUTER, OTHER
  "transportModes": ["ALL"], # HIGHWAY, TRAIN, ROAD
  "interval": "MONTH",
  "aggregationLevel": "MUN" # PLZ, DIS, CAN
}
```

```json
[
  {
    "date": "2019-07-31T22:00:00.000Z",
    "total": 324,
    "weightedTotal": 369,
    "origins": [
      {
        "id": 5017,
        "value": 46,
        "weightedValue": 49
      },
      {
        "id": 5002,
        "value": 144,
        "weightedValue": 157
      }
    ],
    "values": {
      "5002": [144, 157],
      "5017": [46, 49]
    }
  },
  {
    "date": "2019-08-31T22:00:00.000Z",
    "total": 32,
    "weightedTotal": 36,
    "origins": [
      {
        "id": 5002,
        "value": 0,
        "weightedValue": 21
      }
    ],
    "values": {
      "5002": [0, 21]
    }
  }
]
```
